import { atomWithStorage } from "jotai/utils";

export const menuEditAtomStorage = atomWithStorage("edit", {
 id: "",
 name: "",
 price: 0,
 stocks: 0,
});
