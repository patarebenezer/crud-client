import { ApiResponseSuccess } from "./api";

export type GetMenuResponse = ApiResponseSuccess<MenuTypes>;

export type MenuTypes = Partial<{
 id: string;
 name: string;
 price: number;
 stocks: number;
}>;

export type AddMenuTypes = Partial<{
 name: string;
 price: number;
 stocks: number;
}>;
