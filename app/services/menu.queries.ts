import { createQueryKeys } from "@lukemorales/query-key-factory";
import {
 addMenuData,
 deleteMenuData,
 getMenuData,
 updateMenuData,
} from "./menu.services";

export const MenuQueries = createQueryKeys("MenuQueries", {
 menu: () => ({
  queryFn: () => getMenuData(),
  queryKey: ["menu"],
 }),
 addMenu: (formData) => ({
  queryFn: () => addMenuData(formData),
  queryKey: ["addMenu"],
 }),
 updateMenu: (id) => ({
  queryFn: () => updateMenuData(id),
  queryKey: ["updateMenu"],
 }),
 deleteMenu: (id) => ({
  queryFn: () => deleteMenuData(id),
  queryKey: ["deleteMenu"],
 }),
});
