import { AddMenuTypes, GetMenuResponse, MenuTypes } from "../types/MenuTypes";
import { axiosClient } from "./utils/axiosClient";
import { clientEnv } from "../env/client.environment";

export const getMenuData = async () => {
 try {
  const response = await axiosClient.get<GetMenuResponse>(
   `${clientEnv.API_BASE_URL}menu`
  );
  return response.data;
 } catch (error) {
  console.error("error", error);
  throw error;
 }
};

export const addMenuData = async (formData: AddMenuTypes) => {
 try {
  const response = await axiosClient.post<GetMenuResponse>(
   `${clientEnv.API_BASE_URL}menu`,
   formData,
   {
    headers: {
     "Content-Type": "application/json",
    },
   }
  );
  return response.data;
 } catch (error) {
  console.error("error", error);
  throw error;
 }
};

export const updateMenuData = async (formData: MenuTypes) => {
 try {
  const response = await axiosClient.patch<GetMenuResponse>(
   `${clientEnv.API_BASE_URL}menu/${formData.id}`,
   formData,
   {
    headers: {
     "Content-Type": "application/json",
    },
   }
  );
  return response.data;
 } catch (error) {
  console.error("error", error);
  throw error;
 }
};

export const deleteMenuData = async (id: string) => {
 try {
  const response = await axiosClient.delete<GetMenuResponse>(
   `${clientEnv.API_BASE_URL}menu/${id}`
  );
  return response.data;
 } catch (error) {
  console.error("error", error);
  throw error;
 }
};
