import { useQuery } from "@tanstack/react-query";
import { MenuQueries } from "./menu.queries";

export const useMenuData = () => {
 return useQuery({
  ...MenuQueries.menu(),
 });
};
