import type { Metadata } from "next";
import { Roboto_Mono } from "next/font/google";
import "./globals.css";
import Providers from "./utils/provider";
export const roboto_mono = Roboto_Mono({
 subsets: ["latin"],
 display: "swap",
});

export const metadata: Metadata = {
 title: "Menu App",
 description: "Created by Patar E. Siahaan",
};

export default function RootLayout({
 children,
}: {
 children: React.ReactNode;
}) {
 return (
  <html lang='en' className='bg-base-200'>
   <body className={roboto_mono.className}>
    <Providers>{children}</Providers>
   </body>
  </html>
 );
}
