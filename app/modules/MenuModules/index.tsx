"use client";
import { useState } from "react";
import { useMenuData } from "@/app/services/menu.hooks";
import { deleteMenuData } from "@/app/services/menu.services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-toastify";
import { AxiosError } from "axios";
import { ApiResponseError } from "@/app/types/api";
import Breadcrumbs from "../components/Breadcrumbs";
import ModalAdd from "../components/ModalAdd";
import ModalDelete from "../components/ModalDelete";
import { MenuTypes } from "@/app/types/MenuTypes";
import { useSetAtom } from "jotai";
import { menuEditAtomStorage } from "@/app/utils/menuJotai";

export default function MenuModules() {
 const queryClient = useQueryClient();
 const { data } = useMenuData();
 const [itemId, setItemId] = useState("");
 const setSelectedEdit = useSetAtom(menuEditAtomStorage);
 const openModalDelete = (item: string) => {
  setItemId(item);
  const modal = document.querySelector(
   "#modalDelete"
  ) as HTMLDialogElement | null;
  if (modal) {
   modal.showModal();
  }
 };

 const openModalAdd = () => {
  const modal = document.querySelector("#modalAdd") as HTMLDialogElement | null;
  if (modal) {
   modal.showModal();
   setSelectedEdit({
    id: "",
    name: "",
    price: 0,
    stocks: 0,
   });
  }
 };

 const openModalEdit = (item: MenuTypes) => {
  openModalAdd();
  setSelectedEdit({
   id: item?.id as string,
   name: item?.name as string,
   price: item?.price as number,
   stocks: item?.stocks as number,
  });
 };

 const deleteMenuList = useMutation({
  mutationFn: deleteMenuData,
  onSuccess: () => {
   toast.success("Data deleted successfully", { position: "top-right" });
   queryClient.invalidateQueries(["MenuQueries"]);
  },
  onError: (error: AxiosError<ApiResponseError>) => {
   const messageError = error.response?.data.message;
   toast.error(messageError, { position: "top-right" });
  },
 });

 const submitDeleteMenu = () => {
  deleteMenuList.mutate(itemId);
 };

 return (
  <div className='lg:w-1/2 w-full mx-auto px-7 py-5 bg-white h-screen'>
   <Breadcrumbs />
   <h1 className='text-3xl font-bold antialiased mb-10'>Menu</h1>
   <div className='flex justify-end'>
    <button className='btn btn-primary' onClick={() => openModalAdd()}>
     Add Menu +
    </button>
   </div>
   <div className='overflow-x-auto mt-3'>
    <table className='table table-zebra'>
     <thead>
      <tr className='bg-gray-700 text-white text-lg'>
       <th></th>
       <th>Name</th>
       <th>Price</th>
       <th>Stock</th>
       <th>Action</th>
      </tr>
     </thead>
     <tbody>
      {Array.isArray(data) &&
       data?.map((item, index) => (
        <tr key={index}>
         <th>{index + 1}</th>
         <td>{item?.name}</td>
         <td>{item?.price}</td>
         <td>{item?.stocks}</td>
         <tr className='text-blue-500'>
          <td
           className='hover:underline cursor-pointer'
           onClick={() => openModalEdit(item)}
          >
           Edit
          </td>
          <td
           className='hover:underline cursor-pointer'
           onClick={() => openModalDelete(item?.id)}
          >
           Delete
          </td>
         </tr>
        </tr>
       ))}
     </tbody>
    </table>
    {Array.isArray(data) && data?.length < 1 && (
     <p className='text-center mt-5 text-gray-400 text-sm'>
      Data not available
     </p>
    )}
   </div>
   <ModalDelete deleteMenu={submitDeleteMenu} />
   <ModalAdd />
  </div>
 );
}
