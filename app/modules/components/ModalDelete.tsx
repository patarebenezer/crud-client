import React from "react";

type ModalProps = {
 deleteMenu?: () => void;
};

export default function Modal({ deleteMenu }: ModalProps) {
 return (
  <dialog id='modalDelete' className='modal'>
   <div className='modal-box'>
    <h3 className='font-bold text-lg'>Delete menu</h3>
    <p className='py-4 text-gray-500'>
     Are you sure you want to delete the menu?
    </p>
    <div className='modal-action'>
     <form method='dialog'>
      <button className='btn'>Close</button>
      <button className='ml-3 btn btn-error text-white' onClick={deleteMenu}>
       Delete
      </button>
     </form>
    </div>
   </div>
  </dialog>
 );
}
