import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { AxiosError } from "axios";
import { useAtom } from "jotai";
import { toast } from "react-toastify";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { MenuTypes } from "@/app/types/MenuTypes";
import { ApiResponseError } from "@/app/types/api";
import { menuEditAtomStorage } from "@/app/utils/menuJotai";
import { addMenuData, updateMenuData } from "@/app/services/menu.services";

export default function ModalAdd() {
 const [selectedEdit] = useAtom(menuEditAtomStorage);
 const {
  register,
  handleSubmit,
  formState: { errors },
  reset,
  watch,
  setValue,
 } = useForm();

 const queryClient = useQueryClient();
 const watchValue = watch();

 const onCloseModal = () => {
  const modal = document.querySelector("#modalAdd") as HTMLDialogElement | null;
  modal?.close();
  reset();
 };

 const postMenuForm = useMutation({
  mutationFn: addMenuData,
  onSuccess: () => {
   queryClient.invalidateQueries(["MenuQueries"]);
   toast.success("Data added successfully", {
    position: "top-right",
    autoClose: 2000,
   });
   onCloseModal();
  },
  onError: (error: AxiosError<ApiResponseError>) => {
   const messageError = error.response?.data.message;
   toast.error(messageError, { position: "top-right", autoClose: 2000 });
  },
 });

 const updateMenuForm = useMutation({
  mutationFn: updateMenuData,
  onSuccess: () => {
   toast.success("Data updated successfully", { position: "top-right" });
   queryClient.invalidateQueries(["MenuQueries"]);
   onCloseModal();
  },
  onError: (error: AxiosError<ApiResponseError>) => {
   const messageError = error.response?.data.message;
   toast.error(messageError, { position: "top-right" });
  },
 });

 const onSubmit = (data: MenuTypes) => {
  const payload = {
   id: data?.id,
   name: data?.name,
   price: Number(data?.price),
   stocks: Number(data?.stocks),
  };
  const payloadEdit = {
   id: watchValue.id,
   name: watchValue.name,
   price: Number(watchValue.price),
   stocks: Number(watchValue.stocks),
  };
  if (watchValue?.id) {
   updateMenuForm.mutate(payloadEdit);
  } else {
   postMenuForm.mutate(payload);
  }
 };

 useEffect(() => {
  if (selectedEdit.id !== "") {
   setValue("id", selectedEdit.id);
   setValue("name", selectedEdit.name);
   setValue("price", selectedEdit.price);
   setValue("stocks", selectedEdit.stocks);
  }
 }, [
  selectedEdit.id,
  selectedEdit.name,
  selectedEdit.price,
  selectedEdit.stocks,
  setValue,
 ]);

 return (
  <dialog id='modalAdd' className='modal'>
   <div className='modal-box'>
    <h3 className='font-bold text-lg mb-5'>Add Menu</h3>
    <form onSubmit={handleSubmit(onSubmit)} className='grid gap-5'>
     <div>
      {errors.name && (
       <span className='text-red-500 text-sm'>This field is required</span>
      )}
      <label className='input input-bordered flex items-center gap-2'>
       <span className='text-indigo-600'>Name</span>
       <input
        defaultValue={selectedEdit?.name}
        type='text'
        {...register("name", { required: selectedEdit?.id ? false : true })}
        className='grow'
        placeholder='Menu'
       />
      </label>
     </div>

     <div>
      {errors.price && (
       <span className='text-red-500 text-sm'>This field is required</span>
      )}
      <label className='input input-bordered flex items-center gap-2'>
       <span className='text-indigo-600'>Price</span>
       <input
        type='text'
        {...register("price", { required: selectedEdit?.id ? false : true })}
        className='grow'
        placeholder='10000'
       />
      </label>
     </div>

     <div>
      {errors.stocks && (
       <span className='text-red-500 text-sm'>This field is required</span>
      )}
      <label className='input input-bordered flex items-center gap-2'>
       <span className='text-indigo-600'>Stock</span>
       <input
        type='text'
        {...register("stocks", { required: selectedEdit?.id ? false : true })}
        className='grow'
        placeholder='10'
       />
      </label>
     </div>

     <button type='submit' className='ml-3 btn btn-primary text-white'>
      {selectedEdit?.id ? "Update" : "Add"}
     </button>
    </form>
    <div className='modal-action'>
     <button
      className='btn btn-sm btn-circle btn-ghost absolute right-2 top-2 text-lg'
      onClick={() => onCloseModal()}
     >
      ✕
     </button>
    </div>
   </div>
  </dialog>
 );
}
