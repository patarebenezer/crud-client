import React from "react";

export default function Breadcrumbs() {
 return (
  <div className='breadcrumbs'>
   <ul>
    <li className='font-thin'>
     <a>Home</a>
    </li>
    <li className='font-bold'>
     <a>Menu</a>
    </li>
   </ul>
  </div>
 );
}
